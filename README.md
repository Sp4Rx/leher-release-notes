# LeherApp Release notes
This file contains release notes for [Leher](https://play.google.com/store/apps/details?id=com.leher) app.

## v3.10.9

- Crash fix on camera resume(cased due to onStop cam release crash fix)

|Type|File|
|--|--|
|Apk| [3.10.9.apk](/files/3.10.9/3.10.9.apk)|
|Bundle| [3.10.9.aab](/files/3.10.9/3.10.9.aab)|

## v3.10.8

- Always play slected opinion first from everywhere
- Play the first opinon after the last opinion has ended of the current wave

|Type|File|
|--|--|
|Apk| [3.10.8.apk](/files/3.10.8/3.10.8.apk)|
|Bundle| [3.10.8.aab](/files/3.10.8/3.10.8.aab)|

## v3.10.7
- Notification Scroll loading bug fix
- Tap tap events
- Send token to fcm on refresh also
- crash fixes

## v3.10.6

*(Including v3.10.5, v3.10.4)*
- Bug fixes (Including wrong opinion position form wave detail activity)
- Coach Mark for tap tap


## v3.10.3

*(Including v3.10.2, v3.10.1)*
- **Swipe tap tap view**
- **New camera improvent (Default beautification)**
- Camera with fixed resolution for now
- Show thumbnail before video load
- **Migration to androidx and other library updates**
- Youtube player fixed
- **Appsee Removed**

|Type|File|
|--|--|
|Apk| [3.10.3.apk](/files/3.10.3/3.10.3.apk)|
|Bundle| [3.10.3.aab](/files/3.10.3/3.10.3.aab)|


## v3.9.17 (HotFix)

- [Crash fix: Null view](https://www.fabric.io/aurigait9/android/apps/com.leher/issues/5c9c8f17f8b88c296313e3e8)

## v3.9.16

- **New upload opinion service**
- Show pending upload at profile section
- Hide the follow button in case userType is coming as guest
- replace downvote with viewcount
- Feedback form added with remote config
- **ANR watchdog added**
- **Channel selection moved to step 1**
- Radndomised visibility logic of influencer list on feed

## v3.9.15

- Influencer list on feed	
- Mention user feature	
- Facebook sdk updated to 4.41.0	

## v3.9.14

- Fix terms and condition links
- **OpinionType 'uploaded' added for gallery uploads**
- Single reference of feed list and page no in main activity
- Edit profile error message fix
- Send user tags to firebase

## v3.9.13

 - **Hyperlink text in comments**
- **Can post gif without text**
- 2nd attempt on search  bug fix
- **Prevent feed refresh every time**
- **Removed in_app_banner and its functionality**
- Scroll to top when home is pressed 
- Fix: When there are more than 10 opinions, feed card isnt rendered correctly
- Fix: UserName Check button's positioning changes once clicked
- Fix: Empty Username check thows unexpected error. We shlould have a client side validation
- Custom date picker dialog
- Crash fix on fast address typing
- Open User's Profile when when user icon clicked in comments screen
- Remove the option to Follow yourself when the user appear in someone's follower/following list

## v3.9.12

- **Keyboard popup issue fix** 
- Send the latest among media or external link
- Handled fabric issue 480 (Error messeage shown and stopped recording)
- **Can see more than 2 pages in public feed**

## v3.9.11

- Events revamped
- GIf and image support added for comments

## v3.9.10 

- **Appsee added**
- Crashlytics user id set
- Edit wave crash fix
- Faric crash fixes 463, 211, 442
- Bottom sheet fix on new login from splash screen
- **Handle referral after step 1**

## v3.9.9

 - Referral fix
 - Double instance of fb login fix
 - Fabric issue fixes - 442(Downgraded fb sdk version) , 399, 409, 443, 445, 439, 449, 446
 - Welcome activity remote config default value changed
 - **Can select only 1 channel during wave creation**
 - Commented username on profile update
 - **Upload from galley**
 - Disable http logging on production
 - Fix bug:last opinion played if app was minimised and opened from launcher.
 - Bottom bar bug fix

